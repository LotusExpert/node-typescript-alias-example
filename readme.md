# Setup
```npm install``` then ```npm run build```
# Aliases
* Setup your path aliases in tsconfig.json
* Make sure to install the module-alias package and configure it via package.json. This package makes the compiled javascript files to work with aliases.
* Look at typescript files to compare import paths, with and without aliases