import { Options } from '@interfaces/option/options.interface';


/**
 *  import without alias
 *  import { Options } from '../../../interfaces/option/options.interface'
 */

export class SuperUtil {

  constructor(opt: Options) {
    this._options = opt;
  }

  private _options: Options;

  get options() {
    return this._options;
  }

  set options(opt: Options) {
    this._options = opt;
  }

  public countToTen(): void {
    for(let i = 1; i < 11; i++) {
      console.log(`#${i}`);
    }
  }

  public isProduction(): boolean {
    return this._options.production;
  }
}
