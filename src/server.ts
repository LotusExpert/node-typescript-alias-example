import 'module-alias/register';
import { SuperUtil } from '@superutil/superutil.class';
import { Options } from '@interfaces/option/options.interface';

/**
 *  imports without aliases
 *  import { SuperUtil } from './classes/feature/util/superutil.class';
 *  import { Options } from './interfaces/option/options.interface';
 */

const opts: Options = {basePath: '/', production: false};

const superUtil: SuperUtil = new SuperUtil(opts);

console.log(JSON.stringify(superUtil.options));
superUtil.countToTen();
console.log('Are we in production?', superUtil.isProduction())
