export interface Options {
    basePath: string;
    production: boolean;
}